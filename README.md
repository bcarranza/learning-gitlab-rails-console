# README

Check the **Snippets** for this repository for the good stuff. 


## Miscellaneous

> Things that do not quite merit a snippet or that I have not yet turned into a snippet but that I do not want to lose.

```
failed_uploads.where(secret: nil)
```


These two are from the [SAML login fails for accounts with an LDAP identity when LDAP provider is removed](https://gitlab.com/gitlab-org/gitlab/-/issues/358175) issue:

```
# Rails console snippet
userid = User.find_by(username: "mytestuser").id
id = Identity.where(provider: "ldapmain").where(user_id: userid)
id.destroy_all
```

```
# Rails console snippet
ldap_identities = Identity.where(provider: "ldapmain")
ldap_identities.each do | identity |
    puts 'Destroying identity: ' + identity.attributes.to_s
    identity.destroy!
rescue => e
    puts 'This error was generated when destroying identity:\n ' + identity.attributes.to_s + ':\n' + e.to_s
end
```
